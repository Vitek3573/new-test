
import { createRouter, createWebHistory } from 'vue-router'
const Main = () => import("../Modules/Main.vue");
const Basket = () => import("../Modules/Basket.vue");

const routes = [
    {
        path: "/",
        name: "base",
        component: Main,
    },
    {
        path: "/basket",
        name: "basket",
        component: Basket,
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;
