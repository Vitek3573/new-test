import {defineStore} from "pinia";
import { basketStore } from "./Basket.js";

const url = 'https://api.platovcorp.site/product.json'
export const useStore = defineStore('shopStore', {
    state: () => ({
        shop: [],
        activeTab: 1,
    }),
    actions: {
        async getShop() {
            const res = await fetch(url)
            const data = await res.json()
            this.shop = data
        },
        setActiveTab(id) {
            this.activeTab = id;
        },
        itemBasketAdd(object) {
            const basket = basketStore()
            basket.shop.push({...object})
        }
    }
})