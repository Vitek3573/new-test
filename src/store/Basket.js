import {defineStore} from "pinia";

export const basketStore = defineStore('basketStore', {
    state: () => ({
        shop: [],
    }),
    actions: {
        basketDeleteShop(id) {
            this.shop = this.shop.filter((el) => el.id !== id)
        }
    }
})